<?php
/*Stick a redirect on if get param not passed*/
if(!$_GET[id] || $_GET['id']!=='3266')
{
	header('location: ../index.php');
}
 ?>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Scrabble CRUD application - main</title>
<link rel="stylesheet" type="text/css" href="style.css">
</head>
	<body>
<?php require_once "roarclass.php";
	try
	{
		$roarClass = new RoarClass();
		$member = $roarClass->getAllMembers();
		
		/*If the flag action delete and the ID are passed via URL delete*/
		if(isset($_GET['action']) && isset($_GET['id']))
		{
			if($_GET['action'] == "delete")
			{
				$roarClass->deleteMember($_GET['id']);
			}
		}
	}
	
	catch(Exception $ex)
	{
		echo $ex->getMessage();
	}
?>

<div class="container">
	<h2> Scrabble CRUD application</h2>
	<p>
		<a href="form.php?action=create">Create Profile</a>
		<a href="leaderboard.php">Leaderboard</a>
	</p>
	<?php if($member!==false)
	{?>
		<table>
			<thead>
				<tr>
					<th>Member ID</th>
					<th>Name</th>
					<th>Date Joined</th>
					<th>Contact Number</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
					<th>&nbsp;</th>
				</tr>
			</thead>
			<tbody>
			
				<?php foreach($member AS $memberInfo):?>
				<tr>	
					<td><?=$memberInfo->id?></td>
					<td><?=$memberInfo->name?></td>
					<td><?=$memberInfo->date_joined?></td>
					<td><?=$memberInfo->contact_number?></td>
					<td><a href="read.php?id=<?=$memberInfo->id?>">Read Profile</a></td>
					<td><a href="form.php?action=update&id=<?=$memberInfo->id?>">Update Profile</a></td>
					<td><a onclick="return confirm('Are you sure you want to delete this record?');" href="index.php?action=delete&id=<?=$memberInfo->id?>">Delete Profile</a></td>
				</tr>
				<?php endforeach;?>
			<?php
	}
	else
	{?>
		<h2 class="notification">No member information</h2>
	<?php
	}?>
			</tbody>
		</table>
	</body>
</html>
</div>
